$(function () {
    if ($.fn.pGenerator) {
        $('.js-generate-password').pGenerator({
            'bind': 'click',
            'passwordElement': '.b-form__password-field',
            //'displayElement': '#my-display-element',
            'passwordLength': 8,
            'uppercase': true,
            'lowercase': true,
            'numbers': true,
            'specialChars': false,
            'onPasswordGenerated': function (generatedPassword) {
                //alert('My new generated password is ' + generatedPassword);
            }
        });
    }

    $('.js-show-password').on('click', function (e) {
        e.preventDefault();

        var i = $('.b-form__password-field');

        if (i.attr('type') == 'password') {
            i.attr('type', 'text').next().find('i.fa').removeClass('fa-eye').addClass('fa-eye-slash');
        } else {
            i.attr('type', 'password').next().find('i.fa').removeClass('fa-eye-slash').addClass('fa-eye');
        }
    });
});