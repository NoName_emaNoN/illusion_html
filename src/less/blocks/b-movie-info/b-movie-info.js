$(function () {
    /* Включение видео по клику на постер */
    $(document).on('click', '.b-movie-info__trailer-content', function () {
        var container = $(this).hide().next();

        var player = $('<iframe>')
            .attr('src', $(container).data('href'))
            .attr('width', "750")
            .attr('height', "495")
            .attr('frameborder', "0")
            .attr('allowfullscreen', 'true');

        container.append(player).show();
    });

});