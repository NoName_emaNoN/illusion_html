$(function () {
    var cellWidth = 40;
    var i = 0;

    $('.b-schedule__table-item').each(function () {
        var length = parseInt($(this).data('length'));
        var offset = parseInt($(this).data('offset'));

        length = length ? length : 0;
        offset = offset ? offset : 0;

        $(this).css('width', length / 60 * cellWidth);
        $(this).css('left', offset / 60 * cellWidth);
    });
});