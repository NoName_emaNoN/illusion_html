$(function () {
    $(document).on('click', '.b-splasher__film-image', function () {
        var filmId = $(this).closest('.b-splasher__film').data('film-id');

        /* Уберём все созданные плееры, и вновь покажем контент постеров */
        $('.b-trailers__item iframe').remove();
        $('.b-trailers__item-content').show();

        /* Скроем все блоки трейлеров и покажем нужный */
        $('.b-trailers__item').addClass('hidden').filter('[data-film-id="' + filmId.toString() + '"]').removeClass('hidden');
    });
});