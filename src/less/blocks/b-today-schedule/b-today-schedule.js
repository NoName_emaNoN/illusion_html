$(function () {
    var cellWidth = 109;
    var i = 0;

    $('.b-today-schedule__table-item').each(function () {
        $(this).attr('tabindex', i++);

        var length = parseInt($(this).data('length'));
        var offset = parseInt($(this).data('offset'));

        length = length ? length : 0;
        offset = offset ? offset : 0;

        $(this).css('width', length / 60 * cellWidth);
        $(this).css('left', offset / 60 * cellWidth);
    }).popover({
        content: function () {
            var content = $(this).data('source') && $($(this).data('source')).length ? $($(this).data('source')).html() : false;

            return content;
        },
        container: 'body',
        trigger: 'focus',
        animation: true,
        html: true,
        placement: 'bottom'
    });

    $('.b-today-schedule__now').each(function () {
        var offset = parseInt($(this).data('offset'));
        offset = offset ? offset : 0;
        $(this).css('left', offset / 60 * cellWidth);
    });
});