$(function () {
    /* Т.к. у нас весь блок с картинкой кликабелен, нужно перехватить нажатие на кнопку покупки билета и не отправлять событие дальше */
    $(document).on('click', '.b-trailers__item-button', function () {
        location.href = $(this).attr('href');

        return false;
    });

    /* Включение видео по клику на постер */
    $(document).on('click', '.b-trailers__item-content', function () {
        var container = $(this).hide().next();

        var player = $('<iframe>')
            .attr('src', $(container).data('href'))
            .attr('width', "1170")
            .attr('height', "636")
            .attr('frameborder', "0")
            .attr('allowfullscreen', 'true');

        container.append(player).show();
    });
});